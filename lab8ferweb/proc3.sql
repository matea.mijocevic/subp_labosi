
use labprof8;
go

drop procedure  if exists proc3;
go

create procedure proc3 @sifra integer 
as
begin
	begin transaction t1
	set transaction isolation level repeatable read;

	declare @mjera decimal

	begin try
		set lock_timeout -1;
   		select @mjera = test.mjera
		from test where test.sifra = @sifra
		update test set mjera = mjera + @mjera*0.1 where sifra = @sifra;
		
		set lock_timeout 5;
   		select @mjera = test2.mjera
		from test2 where test2.sifra = @sifra
		update test2 set mjera = mjera + @mjera*0.1 where sifra = @sifra;
		
		set lock_timeout 0;
   		select @mjera = test3.mjera
		from test3 where test3.sifra = @sifra
		update test3 set mjera = mjera + @mjera*0.1 where sifra = @sifra;
		commit transaction t1;
	
	end try
	begin catch
		rollback transaction t1;
		if (ERROR_NUMBER() = 1222) --Lock request time out period exceeded
			throw 50501, 'Privremeno zakljucano, pokusajte kasnije.', 1
		else throw
	end catch
end;
go

exec proc3 2

select * from test

