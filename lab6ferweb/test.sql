SELECT XACT_STATE();

select * from montaza
select * from alat, stroj

� montiranje slobodnog alata na stroj ?iji kapacitet nije popunjen (nema kr�enja integitetskih ograni?enja)

EXEC montirajUzTrans '101','S1'

(1 row affected)
(1 row affected)
(1 row affected)


� montiranje slobodnog alata na stroj ?iji je kapacitet popunjen (prekr�eno integitetsko ograni?enje chkKapacitet)

EXEC montirajUzTrans '104','S1'

� montiranje zauzetog (ve? prije montiranog) alata na stroj ?iji kapacitet nije popunjen (prekr�eno integitetsko ograni?enje pkMontaza)

EXEC montirajUzTrans '101','S2'

� montiranje zauzetog (ve? prije montiranog) alata na stroj ?iji je kapacitet popunjen (prekr�eno integitetsko ograni?enje pkMontaza)

EXEC montirajUzTrans '101','S1'


� montiranje nepostoje?eg alata (alata sa �ifrom koja ne postoji) na stroj ?iji kapacitet nije popunjen (prekr�eno integitetsko ograni?enje fkMontazaAlat)

EXEC montirajUzTrans '000','S2'

drop procedure montirajUzTrans

create procedure montirajUzTrans @sifAlat varchar(3), @oznStroj varchar(2)
as
begin try

	begin transaction t1;
	
	begin try
			insert into montaza values (@oznStroj, @sifAlat);
	end try
	begin catch 
		if (ERROR_MESSAGE() like '%pkMontaza%') --integritet
			throw 50511, 'Alat je zauzet', 1
		else throw;
	end catch

	update alat set montiran = 'D' where sifAlat = @sifAlat;
	
	
	begin try
			update stroj set brojMontiranih = brojMontiranih + 1 where oznStroj = @oznStroj
			commit transaction t1;
	end try
	begin catch
		if (ERROR_MESSAGE() like '%chkKapacitet%') --integritet
			throw 50512, 'Kapacitet stroja je popunjen', 1
		else throw
	end catch

end try
begin catch
	rollback transaction t1;
	throw;
end catch
