

-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof8;

CREATE DATABASE labprof8;
GO

USE labprof8;
DROP TABLE IF EXISTS test;
CREATE TABLE test (
  sifra     INTEGER         NOT NULL
, mjera     DECIMAL(5, 1)   NOT NULL

, CONSTRAINT chkMjera CHECK (mjera <= 1000.0)
);
ALTER TABLE test SET (LOCK_ESCALATION=DISABLE);
EXEC sp_indexoption test, disallowpagelocks, 1;

CREATE UNIQUE INDEX ind1Test ON test (sifra) WITH (ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF);

INSERT INTO test VALUES ( 1,  10.0);
INSERT INTO test VALUES ( 2,  20.0);
INSERT INTO test VALUES ( 3,  30.0);
INSERT INTO test VALUES ( 4,  40.0);
INSERT INTO test VALUES ( 5,  50.0);
INSERT INTO test VALUES ( 6,  60.0);
INSERT INTO test VALUES ( 7,  70.0);
INSERT INTO test VALUES ( 8,  80.0);
INSERT INTO test VALUES ( 9,  90.0);
INSERT INTO test VALUES (10, 100.0);


DROP TABLE IF EXISTS test2;
CREATE TABLE test2 (
  sifra     INTEGER         NOT NULL
, mjera     DECIMAL(5, 1)   NOT NULL

, CONSTRAINT chkMjera2 CHECK (mjera <= 1000.0)
);
ALTER TABLE test2 SET (LOCK_ESCALATION=DISABLE);
EXEC sp_indexoption test2, disallowpagelocks, 1;

CREATE UNIQUE INDEX ind1Test2 ON test2 (sifra) WITH (ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF);

INSERT INTO test2 SELECT * FROM test;

DROP TABLE IF EXISTS test3;
CREATE TABLE test3 (
  sifra     INTEGER         NOT NULL
, mjera     DECIMAL(5, 1)   NOT NULL

, CONSTRAINT chkMjera3 CHECK (mjera <= 1000.0)
);
ALTER TABLE test3 SET (LOCK_ESCALATION=DISABLE);
EXEC sp_indexoption test3, disallowpagelocks, 1;

CREATE UNIQUE INDEX ind1Test3 ON test3 (sifra) WITH (ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF);

INSERT INTO test3 SELECT * FROM test;

