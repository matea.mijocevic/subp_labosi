
use labprof9
SET CONTEXT_INFO 7;

select @@spid -- 55


SET LOCK_TIMEOUT -1; 
BEGIN TRANSACTION; 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
--SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SELECT mjera 
FROM test
WHERE sifra = 5

SELECT mjera 
FROM test
WHERE sifra = 12

UPDATE test
SET mjera = 1
WHERE sifra = 5

ROLLBACK TRANSACTION;

