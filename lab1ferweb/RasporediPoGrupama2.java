import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class RasporediPoGrupama2 {
   
   public static void main(String argv[]) {
      Connection connection = otvoriKonekciju();
      Statement stmt = null;
      try {
         stmt = connection.createStatement();
         // otvori resultSet (kursor) s listom jmbg danom po prezimenu i imenu
         ResultSet rs = stmt.executeQuery("SELECT jmbag FROM stud ORDER BY prezStud, imeStud");
         // priredi statement za pozivanje procedure
         CallableStatement cstmt = connection.prepareCall("{call dbo.upisStudenta(?)}");
         // iteriraj za sve n-torke, za svaki jmbg izvr�i proceduru upisStudenta
         while (rs.next()) {
            String jmbag = rs.getString("jmbag").trim();
            // postavi parametar i pozovi proceduru
            cstmt.setString("jmbag", jmbag);
            try {
               cstmt.execute();
            }
            catch (SQLException exception) {
               int errCode = exception.getErrorCode();
               if (errCode == 50501 || errCode == 50502) {
                  continue;  // while (rs.next())
               }
               else {
                  throw exception;
               }   
            }
         }
         System.out.println("Svi studenti su uspje�no rasporedeni u grupe.");
         rs.close();
         stmt.close();
         connection.close();
      }
      catch (SQLException exception1) {
         int errCode = exception1.getErrorCode();
         if (errCode == 50503) {
            ResultSet rs = null;
            try {
               rs = stmt.executeQuery(
                     "SELECT COUNT(*) AS preostalo FROM stud "
                   + "WHERE jmbag NOT IN (SELECT jmbag FROM studGrupa)");
               rs.next();
               int preostalo = rs.getInt("preostalo");
               System.out.println("Nije uspjelo rasporedivanje " + preostalo + " studenata");
            }
            catch (SQLException exception2) {
               ispisiPogresku(exception2);
               System.exit(-1);
            }
         }
         else {
            ispisiPogresku(exception1);
            System.exit(-1);
         }
         // I u slu�aju pogre�ke trebalo bi osloboditi resurse,
         // ali program ovdje ionako zavr�ava.
      }   
   }
   
   private static void ispisiPogresku(SQLException exception) {
      System.out.println(exception.getErrorCode() + "; " 
            + exception.getMessage() + "; " 
            + "State=" + exception.getSQLState());
   }

   private static Connection otvoriKonekciju () {
      // sastavljanje JDBC URL:
      String url =  
              "jdbc:sqlserver://localhost:1433;"   // ovdje staviti svoje vrijednosti
            + "databaseName=labprof1;"
            + "user=sa;"
            + "password=lozinkazabazu;" ;           // staviti svoje vrijednosti

      // u�itavanje i registriranje SQL Server JDBC driver-a
      try {
         // connection = DriverManager.getConnection(connectionString);
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         System.out.println("SQL Server JDBC driver je ucitan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogreska: nije uspjelo ucitavanje JDBC driver-a.");
         System.out.println(exception.getMessage());
         System.exit(-1);
      }
      
      // uspostavljanje konekcije
      Connection conn = null;
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija je uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogreska: nije uspjelo uspostavljanje konekcije.");
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
         System.exit(-1);
      }
      return conn;
   }
}
