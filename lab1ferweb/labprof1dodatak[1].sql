
DROP TABLE IF EXISTS salesOrderItem;
CREATE TABLE salesOrderItem (
         salesOrderID INTEGER NOT NULL
       , salesOrderItemID INTEGER NOT NULL
       , orderQty INTEGER NOT NULL
       , productID INTEGER NOT NULL
       , specialOfferID INTEGER NOT NULL
       , unitPrice DECIMAL(10,2) NOT NULL
       , unitPriceDiscount DECIMAL(10,2) NOT NULL
);

BULK INSERT salesOrderItem FROM 'C:\tmp\salesOrderItem.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);