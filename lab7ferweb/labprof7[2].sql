
-- ako u instanci SQL Servera ve� postoji baza labprof7, obri�ite ju prije izvr�avanja
-- sljede�ih naredbi
-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof7;

CREATE DATABASE labprof7;
GO

ALTER DATABASE labprof7 SET RECOVERY FULL;   -- OVO JE VA�NO!!!

USE labprof7;

CREATE TABLE stoJeObavljeno (
   sessionId INTEGER
 , rbrTrans  INTEGER
 , rbrZapis  INTEGER
 , tekst     CHAR(8000)
);

-- backup device za Log-Tail. Po potrebi prepraviti putanju do datoteke
EXEC sp_addumpdevice 'disk', 'labprof7LogTailDevice', 'c:\sbp\tertiaryStorage\labprof7LogTailDevice';
