--drop procedure  if exists montirajBezTrans;

create procedure montirajBezTrans @sifAlat varchar(3), @oznStroj varchar(2)
as
begin
	begin try 
		insert into montaza values (@oznStroj, @sifAlat);
	end try
	begin catch 
		if (ERROR_MESSAGE() like '%pkMontaza%') 
			throw 50511, 'Alat je zauzet', 1
		else throw
	end catch
	
	update alat set montiran = 'D' where sifAlat = @sifAlat;
	
	begin try
update stroj set brojMontiranih = brojMontiranih + 1 where oznStroj = @oznStroj
	end try
	begin catch
		if (ERROR_MESSAGE() like '%chkKapacitet%') 
			throw 50512, 'Kapacitet stroja je popunjen', 1
		else throw
	end catch

end 
