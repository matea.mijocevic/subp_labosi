-- ako u instanci SQL Servera ve� postoji baza labprof2, obri�ite ju prije izvr�avanja
-- sljede�ih naredbi ili koristite neko drugo ime baze
-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof2;

CREATE DATABASE labprof2;
GO

USE labprof2;
GO


DROP TABLE IF EXISTS salesOrderItem;
CREATE TABLE salesOrderItem (
         salesOrderID INTEGER NOT NULL
       , salesOrderItemID INTEGER NOT NULL
       , orderQty INTEGER NOT NULL
       , productID INTEGER NOT NULL
       , specialOfferID INTEGER NOT NULL
       , unitPrice DECIMAL(10,2) NOT NULL
       , unitPriceDiscount DECIMAL(10,2) NOT NULL
);


DROP TABLE IF EXISTS salesOrder;
CREATE TABLE salesOrder (
         salesOrderID    INTEGER NOT NULL
       , revisionNumber  INTEGER NOT NULL
       , orderDate       DATETIME NOT NULL
       , dueDate         DATETIME NOT NULL
       , shipDate        DATETIME
       , status          INTEGER NOT NULL
       , customerID      INTEGER NOT NULL
       , salesPersonID   INTEGER
       , territoryID     INTEGER
       , comment         CHAR(128)
);



-- prije izvr�avanja podesiti put do datoteka
BULK INSERT salesOrder FROM 'C:\tmp\salesOrder.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);
BULK INSERT salesOrderItem FROM 'C:\tmp\salesOrderItem.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);

