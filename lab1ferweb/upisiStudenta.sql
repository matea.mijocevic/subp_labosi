drop procedure upisStudenta

create procedure upisStudenta @jmbag varchar(10)
as
begin try
	
	if (select SUM(kapacitet) from labprof1.dbo.grupa ) <= (select SUM(brojStud) from labprof1.dbo.grupa)
		throw 50503, 'Sve grupe su vec popunjene', 1
	if isnull((select COUNT(*) from labprof1.dbo.studGrupa where jmbag = @jmbag),0) >= 1
		throw 50501, 'Student je vec upisan u nastavnu grupu', 1
	if isnull((select COUNT(*) from labprof1.dbo.stud where jmbag = @jmbag), 0) = 0 
		throw 50502, 'Student ne postoji', 1


	declare @grupa varchar(3)
	declare @broj int

select top 1 @broj = MIN(brojStud/CONVERT(decimal(4,2), kapacitet)), @grupa = oznGrupa
	from  labprof1.dbo.grupa
	group by oznGrupa
	order by MIN(brojStud/CONVERT(decimal(4,2), kapacitet)), oznGrupa 

	insert into labprof1.dbo.studGrupa values (@jmbag, @grupa)

	update labprof1.dbo.grupa
	set brojStud = brojStud + 1
	where oznGrupa = @grupa

end try
begin catch
throw
end catch
