import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
 
public class ZoviMontirajBezTrans {
   public static void main(String argv[]) {
      Connection connection = otvoriKonekciju();
 
      try {
      connection.setAutoCommit(false);
 
        while (true) {
            String sifAlat = System.console().readLine().trim();
            String oznStroj = System.console().readLine().trim();
            // Statement stmt = connection.createStatement();
         
			
			try {
			    CallableStatement cstmt = connection.prepareCall("{call dbo.montirajBezTrans(?, ?)}");
				cstmt.setString("sifAlat", sifAlat);
				cstmt.setString("oznStroj", oznStroj);
		        cstmt.execute();
				// stmt.executeUpdate("EXEC montirajBezTrans '"+sifAlat+"','"+oznStroj+"'");
				connection.commit();
				System.out.println("Alat je montiran");
            } catch(SQLException e){
				connection.rollback();
				throw e;
			}
		}
      }
      catch (SQLException exception) {
            ispisiPogresku(exception);
            System.exit(-1);
            }
   }
 
   private static void ispisiPogresku(SQLException exception) {
        System.out.println(exception.getErrorCode() + "; "
            + exception.getMessage() + "; "
            + "State=" + exception.getSQLState());
   }
   
   private static Connection otvoriKonekciju () {
        // sastavljanje JDBC URL:
        String url =  
              "jdbc:sqlserver://localhost:1433;"   // ovdje staviti svoje vrijednosti
            + "databaseName=labprof6;"
            + "user=sa;"
            + "password=lozinkazabazu;" ;           // staviti svoje vrijednosti
 
      // ucitavanje i registriranje SQL Server JDBC driver-a
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            System.out.println("SQL Server JDBC driver je uèitan i registriran.");
        } catch (ClassNotFoundException exception) {
            System.out.println("Pogreška: nije uspjelo uèitavanje JDBC driver-a.");
            System.out.println(exception.getMessage());
            System.exit(-1);
        }
     
      // uspostavljanje konekcije
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Konekcija je uspostavljena.");
        } catch (SQLException exception) {
            System.out.println("Pogreška: nije uspjelo uspostavljanje konekcije.");
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
            System.exit(-1);
        }
        return conn;
   }
}
