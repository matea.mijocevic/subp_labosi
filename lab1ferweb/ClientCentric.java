 import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;


public class ClientCentric {
   
   public static void main(String argv[]) {
      Connection connection = otvoriKonekciju();
      Statement stmt = null;
      try {
        stmt = connection.createStatement();
		long startTime = System.currentTimeMillis();
        ResultSet rs = stmt.executeQuery("select * from labprof1.dbo.salesOrderItem	where orderQty < 10 order by orderQty");
		long endTime = System.currentTimeMillis();
		long milis = endTime - startTime;

		int[] zaIspis = new int[9];

		while(rs.next()){
			zaIspis[Integer.parseInt(rs.getString("orderQty").trim())-1]++;
		}
		
		for(int i=0; i<zaIspis.length; i++){
			System.out.format("%-3d%-7d\n", i+1, zaIspis[i]);

		}
		
		System.out.println("Duration: " + milis + " miliseconds.");
        rs.close();
        stmt.close();
        connection.close();
      }
      catch (SQLException exception1) {
		  ispisiPogresku(exception1);
		  System.exit(-1);
         }
      }   
  
   
   private static void ispisiPogresku(SQLException exception) {
      System.out.println(exception.getErrorCode() + "; " 
            + exception.getMessage() + "; " 
            + "State=" + exception.getSQLState());
   }

   private static Connection otvoriKonekciju () {
      // sastavljanje JDBC URL:
      String url =  
              "jdbc:sqlserver://localhost:1433;"   
            + "databaseName=labprof1;"
            + "user=sa;"
            + "password=lozinkazabazu;" ;           

      try {
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         System.out.println("SQL Server JDBC driver je ucitan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogreska: nije uspjelo ucitavanje JDBC driver-a.");
         System.out.println(exception.getMessage());
         System.exit(-1);
      }
      
      // uspostavljanje konekcije
      Connection conn = null;
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija je uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogreska: nije uspjelo uspostavljanje konekcije.");
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
         System.exit(-1);
      }
      return conn;
   }
}
