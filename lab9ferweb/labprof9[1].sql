

-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof9;

CREATE DATABASE labprof9;
GO

USE labprof9;
DROP TABLE IF EXISTS test;
CREATE TABLE test (
  sifra     INTEGER         NOT NULL
, mjera     DECIMAL(5, 1)   NOT NULL

);
ALTER TABLE test SET (LOCK_ESCALATION=DISABLE);
EXEC sp_indexoption test, disallowpagelocks, 1;

DECLARE @i INT = 1;
WHILE @i <= 20
   BEGIN
   INSERT INTO test VALUES(@i, @i*10.)
   SET @i = @i + 1;
   END
GO

CREATE UNIQUE INDEX ind1Test ON test (sifra) WITH (ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF);
