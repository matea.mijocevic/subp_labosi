import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class RasporediPoGrupama1 {
   
   public static void main(String argv[]) {
      Connection connection = otvoriKonekciju();
      try {
         CallableStatement cstmt = connection.prepareCall("{call dbo.rasporediPoGrupama}");
         cstmt.execute();
         System.out.println("Svi studenti su uspje�no upisani.");
         cstmt.close();
         connection.close();
      }
      catch (SQLException exception) {
         ispisiPogresku(exception);
         System.exit(-1);
         // I u slu�aju pogre�ke trebalo bi osloboditi resurse,
         // ali program ovdje ionako zavr�ava.
      }
   }
   
   private static void ispisiPogresku(SQLException exception) {
      System.out.println(exception.getErrorCode() + "; " 
            + exception.getMessage() + "; " 
            + "State=" + exception.getSQLState());
   }
   
   private static Connection otvoriKonekciju () {
      // sastavljanje JDBC URL:
      String url =  
              "jdbc:sqlserver://localhost:1433;"   // ovdje staviti svoje vrijednosti
            + "databaseName=labprof1;"
            + "user=sa;"
            + "password=lozinkazabazu;" ;           // staviti svoje vrijednosti

      // u�itavanje i registriranje SQL Server JDBC driver-a
      try {
         // connection = DriverManager.getConnection(connectionString);
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         System.out.println("SQL Server JDBC driver je u�itan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogre�ka: nije uspjelo u�itavanje JDBC driver-a.");
         System.out.println(exception.getMessage());
         System.exit(-1);
      }
      
      // uspostavljanje konekcije
      Connection conn = null;
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija je uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogre�ka: nije uspjelo uspostavljanje konekcije.");
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
         System.exit(-1);
      }
      return conn;
   }
}
