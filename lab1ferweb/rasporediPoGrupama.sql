drop procedure rasporediPoGrupama

create procedure rasporediPoGrupama
as
begin

	declare @jmbag as varchar(10)
	declare @studentCursor as cursor

	set @studentCursor = cursor for select jmbag from labprof1.dbo.stud
	open @studentCursor

	fetch next from @studentCursor INTO @jmbag

	while @@FETCH_STATUS = 0
	begin try
		exec upisStudenta @jmbag
		fetch next from @studentCursor INTO @jmbag
	end try
	begin catch
		if ERROR_NUMBER() = 50501 or ERROR_NUMBER() = 50502
		begin
			fetch next from @studentCursor INTO @jmbag
			continue
		end
		else
		begin
			if ERROR_NUMBER() = 50503
			begin
				declare @brojNerasporedenih int
				set @brojNerasporedenih =  (select COUNT(*) from labprof1.dbo.stud) - (select COUNT(*) from labprof1.dbo.studGrupa)

				declare @tekst varchar(50)
				set @tekst = concat('Nije uspjelo rasporedivanje ', @brojNerasporedenih, ' studenata')
				if @brojNerasporedenih != 0
				throw 50504, @tekst , 1
			end
			else throw
		end
	end catch

	close @studentCursor
	deallocate @studentCursor

end