-- ako u instanci SQL Servera ve� postoji baza labprof5, obri�ite ju prije izvr�avanja
-- sljede�ih naredbi ili koristite neko drugo ime baze
-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof5;

CREATE DATABASE labprof5;
GO

USE labprof5;
GO

ALTER DATABASE labprof5 SET AUTO_UPDATE_STATISTICS OFF;
ALTER DATABASE labprof5 SET AUTO_CREATE_STATISTICS OFF;


DROP TABLE IF EXISTS mjesto;
CREATE TABLE mjesto (
  pbr             INTEGER       NOT NULL 
, naz             CHAR(50)      NOT NULL 
);

DROP TABLE IF EXISTS stud;
CREATE TABLE stud (
  mbr             INTEGER       NOT NULL 
, ime             CHAR(25)      NOT NULL 
, prez            CHAR(25)      NOT NULL
, pbrs            INTEGER       NOT NULL
);

DROP TABLE IF EXISTS ispit;
CREATE TABLE ispit (
  mbri            INTEGER       NOT NULL 
, sifPred         INTEGER       NOT NULL
, ocj             INTEGER       NOT NULL 
);


-- prije izvr�avanja podesiti put do datoteka
BULK INSERT mjesto FROM 'C:\tmp\mjesto.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS, CODEPAGE='1250');
BULK INSERT stud FROM 'C:\tmp\stud.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS, CODEPAGE='1250');
BULK INSERT ispit FROM 'C:\tmp\ispit.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);

