import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;

public class Client {
   public static void main(String argv[]) {
      String url = null;
      Connection conn = null;
      PreparedStatement pstmt = null;
      int sessionId;
       // sastavljanje JDBC URL:
      url =  
              "jdbc:sqlserver://localhost:1433;"   // ovdje staviti svoje vrijednosti
            + "databaseName=labprof7;"
            + "user=sa;"
            + "password=lozinkazabazu;" ;           // ovdje staviti svoje vrijednosti

      // u�itavanje i registriranje SQL Server JDBC driver-a
      try {
         // connection = DriverManager.getConnection(connectionString);
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         System.out.println("SQL Server JDBC driver je u�itan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogre�ka: nije uspjelo u�itavanje JDBC driver-a.");
         System.out.println(exception.getMessage());
         System.exit(-1);
      }
      
      // uspostavljanje konekcije
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija je uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogre�ka: nije uspjelo uspostavljanje konekcije.");
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
         System.exit(-1);
      }

      try {
         ResultSet rs = conn.createStatement().executeQuery("SELECT @@spid");
         rs.next();
         sessionId = rs.getInt(1);

         conn.setAutoCommit(false);
         pstmt = conn.prepareStatement("INSERT INTO stoJeObavljeno VALUES (?, ?, ?, ?)");
      } catch (SQLException exception) {
         System.out.println("Pogreska: nije uspjelo prevodjenje SQL naredbe.");
         exception.printStackTrace();
         return;
      }
      try {
         // obrisi eventualne stare zapise iz relacije stoJeObavljeno
         Statement statement = conn.createStatement();
         statement.executeUpdate("DELETE FROM stoJeObavljeno");
         System.out.println("Obrisani svi zapisi iz relacije stoJeObavljeno");
         
         int rbrTrans = 0;
         while (true) {
            rbrTrans++;
            for (int rbrZapis = 1; rbrZapis <= 20; ++rbrZapis) {
               pstmt.setInt(1, sessionId);
               pstmt.setInt(2, rbrTrans);
               pstmt.setInt(3, rbrZapis);
               LocalTime localTime = LocalTime.now();
               String tekst = "Session:" + sessionId + ". Vrijeme:" +  localTime + ". Transakcija " + rbrTrans + ". Zapis " + rbrZapis + ".";
               pstmt.setString(4, tekst);
               pstmt.executeUpdate();
               System.out.println(tekst);
               try {
                  Thread.sleep(200);
               }
               catch (Exception e) {
                  // ne ocekuje se da ce se ovdje dogoditi exception
               }
            }
            conn.commit();
            System.out.println("Transakcija " + rbrTrans + ". = committed.");
         }  
      } catch (SQLException exception) {
         System.out.println("Pogreska: pri postavljanju parametara ili izvrsavanju SQL naredbe.");
		 System.out.println(exception);
         return;
      }
   }
}
