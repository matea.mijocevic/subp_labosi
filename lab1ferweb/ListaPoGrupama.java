import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ListaPoGrupama {
   
   public static void main(String argv[]) {
      Connection connection = otvoriKonekciju();
      Statement stmt = null;
      try {
         stmt = connection.createStatement();
         ResultSet rs = stmt.executeQuery("select oznGrupa oznG, stud.jmbag jmbag, imeStud ime, prezStud prez from labprof1.dbo.studGrupa join labprof1.dbo.stud as stud on stud.jmbag = studGrupa.jmbag order by oznG, prez, ime");
         while (rs.next()) {
			 System.out.format("%-5s%-12s%-15s\n", rs.getString("oznG").trim(), rs.getString("jmbag").trim(),  rs.getString("ime").trim() + " " + rs.getString("prez"));
        }
         rs.close();
         stmt.close();
         connection.close();
      }
      catch (SQLException exception1) {
		  ispisiPogresku(exception1);
		  System.exit(-1);
         }
      }   
  
   
   private static void ispisiPogresku(SQLException exception) {
      System.out.println(exception.getErrorCode() + "; " 
            + exception.getMessage() + "; " 
            + "State=" + exception.getSQLState());
   }

   private static Connection otvoriKonekciju () {
      // sastavljanje JDBC URL:
      String url =  
              "jdbc:sqlserver://localhost:1433;"   // ovdje staviti svoje vrijednosti
            + "databaseName=labprof1;"
            + "user=sa;"
            + "password=lozinkazabazu;" ;           // staviti svoje vrijednosti

      // uèitavanje i registriranje SQL Server JDBC driver-a
      try {
         // connection = DriverManager.getConnection(connectionString);
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         System.out.println("SQL Server JDBC driver je ucitan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogreska: nije uspjelo ucitavanje JDBC driver-a.");
         System.out.println(exception.getMessage());
         System.exit(-1);
      }
      
      // uspostavljanje konekcije
      Connection conn = null;
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija je uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogreska: nije uspjelo uspostavljanje konekcije.");
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
         System.exit(-1);
      }
      return conn;
   }
}
