
use labprof9

--koji kljucevi su postavljeni na koje objekte povezane s relacijom test
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT request_session_id AS sid , 'KEY' AS object , request_mode AS lock_type , CAST (test.sifra AS VARCHAR) AS identifikator 
FROM sys.dm_tran_locks LEFT OUTER JOIN test WITH(NOLOCK INDEX(ind1Test)) 
ON test.%%lockres%% = resource_description 
WHERE resource_type = 'KEY' AND request_status = 'GRANT' 
AND request_session_id IN (SELECT session_id FROM sys.dm_exec_sessions where CAST(context_info AS INT) = 7) 
UNION 
SELECT request_session_id AS sid , 'ROW' AS object , request_mode AS lock_type , CAST (test.sifra AS VARCHAR) AS identifikator 
FROM sys.dm_tran_locks LEFT OUTER JOIN test 
ON sys.fn_PhysLocFormatter(%%physloc%%) = '(' + TRIM(resource_description) + ')' 
WHERE resource_type = 'RID' AND request_status = 'GRANT' 
AND request_session_id 
IN (SELECT session_id FROM sys.dm_exec_sessions where CAST(context_info AS INT) = 7) 
UNION 
SELECT request_session_id AS sid , 'TABLE' AS object , request_mode AS lock_type , sys.objects.name 
FROM sys.dm_tran_locks LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.dm_tran_locks.resource_associated_entity_id
WHERE resource_type = 'OBJECT' AND request_status = 'GRANT' AND request_session_id 
IN (SELECT session_id FROM sys.dm_exec_sessions where CAST(context_info AS INT) = 7);

--koja transakcija (sjednica) eventualno ceka zbog kljuceva koje je postavila neka druga transakcija
SELECT session_id AS sid_blocked , blocking_session_id AS sid_blocking 
FROM sys.dm_os_waiting_tasks WHERE blocking_session_id 
IN (SELECT session_id FROM sys.dm_exec_sessions WHERE CAST(context_info AS INT) = 7);