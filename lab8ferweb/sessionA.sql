
use labprof8
SET CONTEXT_INFO 7;

select @@spid -- 52 = sessionA 60

SET LOCK_TIMEOUT -1; 
BEGIN TRANSACTION; 
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

--t1 read 1, 2
CREATE TABLE #variables(
    sifra int,	mjera decimal
    )

Insert into #variables
SELECT sifra, mjera 
FROM test
WHERE sifra = 5 or sifra = 6

--t1 write1 

UPDATE test
SET mjera = (select mjera from #variables where sifra = 6)
WHERE sifra = 5

-- ide t2

--t1 write2

UPDATE test
SET mjera = (select mjera from #variables where sifra = 5)
WHERE sifra = 6

delete #variables

COMMIT TRANSACTION;

SELECT mjera 
FROM test
WHERE sifra = 5 or sifra = 6 

select * from #variables
