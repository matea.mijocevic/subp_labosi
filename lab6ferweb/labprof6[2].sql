
-- ako u instanci SQL Servera ve� postoji baza labprof6, obri�ite ju prije izvr�avanja
-- sljede�ih naredbi ili koristite neko drugo ime baze
-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof6;

CREATE DATABASE labprof6;
GO

USE labprof6;

DROP TABLE IF EXISTS stroj;
CREATE TABLE stroj (
  oznStroj        CHAR(10)      NOT NULL
, kapacitet       INTEGER       NOT NULL  -- najve�i broj alata koji se smije montirati na ovaj stroj
, brojMontiranih  INTEGER       NOT NULL  -- koliko alata je trenuta�no montirano na ovom stroju

, CONSTRAINT pkStroj PRIMARY KEY (oznStroj) 
, CONSTRAINT chkKapacitet CHECK (brojMontiranih <= kapacitet)
); 


DROP TABLE IF EXISTS alat;
CREATE TABLE alat (
  sifAlat         INTEGER       NOT NULL
, montiran        CHAR(1)       NOT NULL   -- ovaj alat jest trenuta�no montiran na nekom stroju (D/N) 

, CONSTRAINT pkAlat PRIMARY KEY (sifAlat) 
);


DROP TABLE IF EXISTS montaza;
CREATE TABLE montaza (
  oznStroj        CHAR(10)      NOT NULL   -- na ovom stroju
, sifAlat         INTEGER       NOT NULL   -- trenuta�no je montiran ovaj alat

, CONSTRAINT pkMontaza PRIMARY KEY (sifAlat) 
, CONSTRAINT fkMontazaStroj FOREIGN KEY (oznStroj) REFERENCES stroj (oznStroj) 
, CONSTRAINT fkMontazaAlat  FOREIGN KEY (sifAlat) REFERENCES alat (sifAlat) 
);

-- punjenje test podacima

INSERT INTO stroj VALUES('S1', 3, 2);
INSERT INTO stroj VALUES('S2', 2, 0);
INSERT INTO stroj VALUES('S3', 2, 1);
INSERT INTO stroj VALUES('S4', 1, 1);
INSERT INTO stroj VALUES('S5', 2, 0);
INSERT INTO stroj VALUES('S6', 3, 0);

INSERT INTO alat VALUES(101, 'N');
INSERT INTO alat VALUES(103, 'D');
INSERT INTO alat VALUES(104, 'N');
INSERT INTO alat VALUES(121, 'D');
INSERT INTO alat VALUES(130, 'N');
INSERT INTO alat VALUES(133, 'N');
INSERT INTO alat VALUES(134, 'D');
INSERT INTO alat VALUES(137, 'N');
INSERT INTO alat VALUES(154, 'D');

INSERT INTO montaza VALUES('S1', 134);
INSERT INTO montaza VALUES('S1', 154);
INSERT INTO montaza VALUES('S3', 103);
INSERT INTO montaza VALUES('S4', 121);

