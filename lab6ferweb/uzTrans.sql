--drop procedure montirajUzTrans

create procedure montirajUzTrans @sifAlat varchar(3), @oznStroj varchar(2)
as
begin try
	begin transaction t1;
	
	begin try
			insert into montaza values (@oznStroj, @sifAlat);
	end try
	begin catch 
		if (ERROR_MESSAGE() like '%pkMontaza%') --integritet
			throw 50511, 'Alat je zauzet', 1
		else throw;
	end catch

	update alat set montiran = 'D' where sifAlat = @sifAlat;
	
	
	begin try
			update stroj set brojMontiranih = brojMontiranih + 1 where oznStroj = @oznStroj
			commit transaction t1;
	end try
	begin catch
		if (ERROR_MESSAGE() like '%chkKapacitet%') --integritet
			throw 50512, 'Kapacitet stroja je popunjen', 1
		else throw
	end catch

end try
begin catch
	rollback transaction t1;
	throw;
end catch
