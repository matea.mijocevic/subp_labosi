-- ako u instanci SQL Servera ve� postoji baza labprof3, obri�ite ju prije izvr�avanja
-- sljede�ih naredbi ili koristite neko drugo ime baze
-- USE master;
-- GO
-- DROP DATABASE IF EXISTS labprof3;

CREATE DATABASE labprof3;
GO

USE labprof3;
GO

ALTER DATABASE labprof3 SET AUTO_UPDATE_STATISTICS OFF;
ALTER DATABASE labprof3 SET AUTO_CREATE_STATISTICS OFF;



DROP TABLE IF EXISTS salesOrderItem;
CREATE TABLE salesOrderItem (
         salesOrderID INTEGER NOT NULL
       , salesOrderItemID INTEGER NOT NULL
       , orderQty INTEGER NOT NULL
       , productID INTEGER NOT NULL
       , specialOfferID INTEGER NOT NULL
       , unitPrice DECIMAL(10,2) NOT NULL
       , unitPriceDiscount DECIMAL(10,2) NOT NULL
);


DROP TABLE IF EXISTS salesOrder;
CREATE TABLE salesOrder (
         salesOrderID    INTEGER NOT NULL
       , revisionNumber  INTEGER NOT NULL
       , orderDate       DATETIME NOT NULL
       , dueDate         DATETIME NOT NULL
       , shipDate        DATETIME
       , status          INTEGER NOT NULL
       , customerID      INTEGER NOT NULL
       , salesPersonID   INTEGER
       , territoryID     INTEGER
       , comment         CHAR(128)
);


DROP TABLE IF EXISTS pointsInArea1;
CREATE TABLE pointsInArea1 (
         pointID         INTEGER NOT NULL  
       , x               DECIMAL(6,2) NOT NULL
       , y               DECIMAL(6,2) NOT NULL
       , z               INTEGER NOT NULL
);


DROP TABLE IF EXISTS pointsInArea2;
CREATE TABLE pointsInArea2 (
         pointID         INTEGER NOT NULL  
       , x               DECIMAL(6,2) NOT NULL
       , y               DECIMAL(6,2) NOT NULL
       , z               INTEGER NOT NULL
);


DROP TABLE IF EXISTS pointsInArea3;
CREATE TABLE pointsInArea3 (
         pointID         INTEGER NOT NULL  
       , x               DECIMAL(6,2) NOT NULL
       , y               DECIMAL(6,2) NOT NULL
       , z               INTEGER NOT NULL
);

-- prije izvr�avanja podesiti put do datoteka
BULK INSERT salesOrder FROM 'C:\tmp\salesOrder.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);
BULK INSERT salesOrderItem FROM 'C:\tmp\salesOrderItem.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);
BULK INSERT pointsInArea1 FROM 'C:\tmp\pointsInArea1.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);
BULK INSERT pointsInArea2 FROM 'C:\tmp\pointsInArea2.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);
BULK INSERT pointsInArea3 FROM 'C:\tmp\pointsInArea3.csv' WITH (FIELDTERMINATOR = ';', KEEPNULLS);

