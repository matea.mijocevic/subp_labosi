
select session_id, last_request_start_time, db.name 
from sys.dm_exec_sessions as ss join sys.databases db
on ss.database_id = db.database_id
where ss.context_info = 7

select * from sys.databases

select * from sys.dm_exec_sessions



--provjeru koji kljucevi su postavljeni na koje n-torke relacije test
use labprof8; 
go

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT request_session_id AS sid
, CASE WHEN request_mode = 'U' THEN 'S' ELSE request_mode END AS lock_type
, test.sifra
FROM sys.dm_tran_locks
LEFT OUTER JOIN test
ON sys.fn_PhysLocFormatter(%%physloc%%) = '(' + TRIM(resource_description) + ')'
WHERE resource_type = 'RID'
AND request_status = 'GRANT'
AND request_session_id IN (SELECT session_id FROM sys.dm_exec_sessions WHERE CAST(context_info AS INT) = 7);
go


--provjeru koja transakcija (sjednica) eventualno ceka zbog kljuceva koje je postavila neka druga transakcija (sjednica)
SELECT session_id AS sid_blocked
, blocking_session_id AS sid_blocking
FROM sys.dm_os_waiting_tasks
WHERE blocking_session_id IN (SELECT session_id
FROM sys.dm_exec_sessions
WHERE CAST(context_info AS INT) = 7);


